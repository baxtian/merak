<?php

namespace Baxtian;

// Declarar la interfaz 'MerakInterface'
interface MerakInterface
{
	/**
	 * Retornar la URL del directorio del plugin / tema.
	 */
	public function url_path(): string;

	/**
	 * Retornar el path del directorio del plugin / tema.
	 */
	public function dir_path(): string;

	/**
	 * Retorna el path del directorio i18n del plugin / tema.
	 */
	public function languages_dir(): string;
}
