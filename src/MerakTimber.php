<?php

namespace Baxtian;

use Twig\Environment;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Twig\Error\RuntimeError;
use Timber\Timber;
use Timber\ImageHelper;
use Timber\Image;
use Exception;

// Inicializar Plugin
class MerakTimber
{
	use \Baxtian\SingletonTrait;

	/**
	 * Retorna una cadena con las clases. Los argumentos se concatenan de manera
	 * condicional. Ejemplo:
	 * html_classes(
	 * 		'a-class',
	 * 		'another-class', {
	 * 			'errored': object.errored,
	 * 			'finished': object.finished,
	 * 			'pending': object.pending,
	 * 		}
	 * 	).
	 *
	 * @param array $args Arreglo con las clases a ser concatenadas
	 *
	 * @return string Cadena con las clases
	 */
	public function html_classes(...$args): string
	{
		// Eliminar vacios
		$args = array_filter($args);

		$classes = [];
		foreach ($args as $i => $arg) {
			if (\is_string($arg)) {
				$classes[] = $arg;
			} elseif (\is_array($arg)) {
				foreach ($arg as $class => $condition) {
					if (!\is_string($class)) {
						throw new RuntimeError(sprintf('The html_classes function argument %d (key %d) should be a string, got "%s".', $i, $class, \gettype($class)));
					}
					if (!$condition) {
						continue;
					}
					$classes[] = $class;
				}
			} else {
				throw new RuntimeError(sprintf('The html_classes function argument %d should be either a string or an array, got "%s".', $i, \gettype($arg)));
			}
		}

		return implode(' ', array_unique($classes));
	}

	public function date_i18n($timestamp, $format = false)
	{
		global $offset_time;

		if (!isset($offset_time)) {
			$offset_time = get_option('gmt_offset') * 60 * 60;
		}

		if ($format === false) {
			$format = trim(sprintf('%s %s', get_option('date_format'), get_option('time_format')));
			if (empty($format)) {
				$format = 'c';
			}
		}

		$timestamp = $offset_time + $timestamp;

		return date_i18n($format, $timestamp);
	}

	/**
	 * Eliminar tabuladores de un texto.
	 *
	 * @param string $string texto al que se eliminarán los tabuladores y los espacios extra
	 *
	 * @return string texto formateado
	 */
	public function skip_tabs($string)
	{
		$string = preg_replace('/\t/', '', $string);
		$string = preg_replace('/\s+/', ' ', $string);

		return trim(str_replace(PHP_EOL, '', $string));
	}

	/**
	 * Crear los estilos para el srcset de una imagen en background.
	 * ***********************
	 * Si va a usar lazyload recuerde incluir la clase lazyload en el div.
	 * ***********************
	 * param  int|TimberImage $img      Identificador de la imagen o el objeto TimberImage.
	 *
	 * @param string $id       id del div al que se aplicarán los estilos
	 * @param array  $srcset   Conjunto de reglas srcset. Cada filacorresponde a un srcset,
	 *                         y contendrá 3 items. El primero es el ancho máximo, el segundo
	 *                         el ancho de la imagen y el tercero el alto.
	 *                         Ejemplo: [
	 *                         [2000, 1000, 0],
	 *                         [1500, 750, 0],
	 *                         [1000, 500, 0],
	 *                         [600, 600, 0]
	 *                         ]
	 * @param bool   $lazyload En caso afirmativo, usar en las reglas la clase .lazyload para que así
	 *                         la imagen solo se cargue en caso que el div tenga esa clase. Esta opción
	 *                         se usa cuando el plugin Autoptimize esté disponible.
	 *
	 * @return string|false URL de la imagen o false si no pudo crear la imagen
	 */
	public function srcset_style($img, $id, $srcset, $lazyload = false)
	{
		//Si el item es un número, entonces obtenr el elemento
		if (is_numeric($img)) {
			$img = Timber::get_post($img);
		}

		//Detectar que se imagen, sino, retornar vacío
		if (!isset($img->post_type) || $img->post_type != 'attachment') {
			return false;
		}

		$args = [
			'image'       => $img,
			'id'          => $id,
			'srcset'      => $srcset,
			'autoptimize' => $lazyload,
		];

		return $this->render('partial/srcset_style.twig', $args, false);
	}

	/**
	 * Lista de entradas relacionadas. Si está instalado Custom Related Posts Premium
	 * se usará la respuesta de es eplugin. Si no se buscaán las últimas notas que tengan
	 * las mismas categorías, excluyendo la nota actual.
	 *
	 * @param int|TimberPost $post objeto TimberPost o id de la entrada
	 * @param array          $args Argumentos para el buscador.
	 *                             Valores por defecto:
	 *                             post_per_page: 4
	 *
	 * @return array|false Arreglo con los posts relacionados
	 */
	public function related_posts($post, $args = [])
	{
		if (!$post) {
			$post = get_post();
		}

		$args = wp_parse_args($args, [
			'posts_per_page' => 4,
		]);

		extract($args);

		$answer = false;

		//Si está instalado el plugin Custom Related Posts, usarlo
		if (class_exists('CustomRelatedPostsPremium')) {
			$crp    = \CustomRelatedPosts::get();
			$answer = $crp->relations_to($post->ID);
		} else { //Si no está instalado el plugin, usar las últimas de las mismas categorías
			$cats = wp_get_post_categories($post->ID);
			$args = [
				'posts_per_page' => $posts_per_page,
				'category'       => implode(',', $cats),
				'post__not_in'   => [$post->ID],
			];
			$answer = Timber::get_posts($args);
		}

		return $answer;
	}

	/**
	 * Formatear un texto aplicando el filtro the_content.
	 *
	 * @param string $string Texto a ser formateado
	 *
	 * @return string texto formateado
	 */
	public function format($string)
	{
		return nl2br(apply_filters('the_content', $string));
	}

	/**
	 * Filtro para insertar la URL de una imagen fly en Timber
	 * @param  int|TimberImage $item   Identificador de la imagen o el objeto TimberImage.
	 * @param  int             $width  Ancho de la imagen.
	 * @param  int|false       $height Alto de la imagen o false para calcular manteniendo la relación.
	 * @return string|false            URL de la imagen o false si no pudo crear la imagen.
	 */
	public function adjust($item, $width, $height = false)
	{
		//Si el item es un número, entonces obtener el objeto TimberPost a partir del id.
		if (is_numeric($item)) {
			$item = Timber::get_post($item);
		}

		//Detectar que sea imagen, sino, retornar vacío
		if (!isset($item->post_type) || $item->post_type != 'attachment') {
			return false;
		}

		//Buscar el tamaño
		$crop = true;
		$size = $width; //Podría ser el descriptor del tamaño
		if (is_numeric($width)) { //Si es un número, entonces es por el momento solo el ancho
			if ($height === false || $height == 0) {
				//Si no hay ancho o es 0, entonces la altura se calcula según la
				//relación WxH de la imagen por defecto
				$size = [$width, 0];
				$crop = false;
			} elseif (is_numeric($height)) {
				// Si ambos datos son numércos entonces debemos recortar la imagen
				// según el tamaño indicado
				$size = [$width, $height];
				$crop = true;
			}
		}

		// Buscar la imagen con el tamaño solicitado
		$answer = '';
		if (function_exists('fly_get_attachment_image_src')) {
			// Si tenemos fly-image-resizer, usar este plugin
			$answer = \fly_get_attachment_image_src($item->id, $size, $crop);
			if (is_array($answer)) {
				$answer = $answer['src'];
			}
		} else {
			// Si no tenemos fly image resizer, usar Timber
			$img    = Timber::get_post($item->id);
			$answer = ImageHelper::resize($img->src, $size[0], $size[1], 'center');
			if (is_array($answer)) {
				$answer = $answer[0];
			}
		}

		// Buscamos la URL en la respuesta
		// if (is_array($answer)) {
		// 	$answer = $answer[0];
		// }

		// Si hay hotspot_timestamp entonces ponerlo al final para 
		// forzar usar la imagen con el hotspot aplicado

		if ($timestamp = get_post_meta($item->ID, 'hotspot_timestamp', true)) {
			$answer .= '?timestamp=' . $timestamp;
		}

		return $answer;
	}

	/**
	 * Integra pagenavi a Timber.
	 *
	 * @param array $args Argumentos para el paginador.
	 *                    Valores por defecto:
	 *                    query: $GLOBALS['wp_query'] // Cambie el valor de query si es un paginador para un get_posts
	 *
	 * @return string HTML del paginador
	 */
	public function pagenavi($args = [])
	{
		$args = shortcode_atts(
			[
				'query' => $GLOBALS['wp_query'],
				'type'  => 'posts',
				'echo'  => false,
				'pages' => false,
			],
			$args
		);

		global $numpages;
		$aux_numpages = $numpages;

		//Si sabemos que hay páginas, entonces hay que usar un multipart
		if ($args['pages']) {
			wp_reset_query();
			$args['type'] = 'multipart';
			$numpages     = $args['pages'];
		}

		$answer = '';
		if (function_exists('wp_pagenavi')) {
			$answer = \wp_pagenavi($args);
		}

		$numpages = $aux_numpages;

		return $answer;
	}

	/**
	 * Función para tener un arreglo con los enlaces de los padres de la nota/página actual.
	 *
	 * @param array $args Argumentos para la miga de pan.
	 *                    Valores por defecto:
	 *                    home: 'Home', //Texto del enlace para la página de inicio
	 *
	 * @return array arreglo con los enlaces en orden
	 */
	public function breadcrumb($args = [])
	{
		$args = shortcode_atts(
			[
				'home' => __('Home', 'merak'),
			],
			$args
		);

		//Inicializar con la página de inicio
		$breadcrumbs = [
			[
				'title' => $args['home'],
				'link'  => get_bloginfo('url'),
			],
		];

		// Si es una categoría usar el nombre de la categoría
		if(is_category() || is_tag() || is_tax()) {
			$term = get_queried_object();
			$tax = get_taxonomy($term->taxonomy);
			$breadcrumbs[] = [
				'title' => $tax->labels->singular_name,
				'link'  => '',
			]; 

			$breadcrumbs[] = [
				'title' => $term->name,
				'link'  => '',
			];

			return $breadcrumbs;
		}

		//Recorrer los ancestros
		wp_reset_query();
		global $post;
		$parents = get_post_ancestors($post->ID);
		foreach ($parents as $parent) {
			$breadcrumbs[] = [
				'title' => get_the_title($parent),
				'link'  => get_the_permalink($parent),
			];
		}

		// Listado de custom post types
		$args = [
			'public'   => true,
			'_builtin' => false, // Use false to return only custom post types
		];
		$custom_post_types = get_post_types($args);

		// Si es un custom post type, usar la página archive
		if ($post->post_type == 'post' || in_array($post->post_type,  array_keys($custom_post_types))) {
			global $wp_post_types;
			$obj = $wp_post_types[$post->post_type];

			$breadcrumbs[] = [
				'title' => $obj->labels->singular_name,
				'link'  => get_post_type_archive_link($post->post_type),
			];
		}

		//Terminar con la página actual
		if (is_singular()) {
			$breadcrumbs[] = [
				'title' => get_the_title($post->ID),
				'link'  => get_the_permalink($post->ID),
			];
		}

		// Build our temporary array (pieces of bread) into one big string :)
		return apply_filters('merak/breadcrumbs', $breadcrumbs, $args);
	}

	/**
	 * Función para imprimir un bloque reusable en twig
	 *
	 * @param string $id Nombre, slug o id del bloque reutilizable
	 *
	 * @return string bloque renderizado
	 */
	public function reusable_block($id): string
	{
		$answer = sprintf('<!-- %s: %s -->', __('Non-existent block', 'merak'), $id);

		// Si es un número hacer el llamado directo
		if (is_numeric($id)) {
			$text = sprintf('<!-- wp:block {"ref":%d} /-->', $id);

			return do_blocks($text);
		}

		// Para evitar hacer múltiples consutas a la base de datos
		// se usará un arreglo global donde se almacenará la información de todos los bloques reusables
		// Este arreglo será instanciado una vez y se seguirá usando en consultas posteriores
		global $bloques_reusables;

		// Si no está creado el arreglo, consultar todos los bloques
		if (empty($bloques_reusables)) {
			$bloques = get_posts([
				'post_type' => 'wp_block',
				'nopaging'  => true,
			]);

			foreach ($bloques as $bloque) {
				$bloques_reusables[$bloque->post_title] = $bloque->ID;
				$bloques_reusables[$bloque->post_name]  = $bloque->ID;
			}
		}

		// Si existe la referencia, usarla
		if (!empty($bloques_reusables[$id])) {
			$text = sprintf('<!-- wp:block {"ref":%d} /-->', $bloques_reusables[$id]);

			return do_blocks($text);
		}

		return $answer;
	}

	/**
	 * Función para extender los filtros de Twig.
	 *
	 * @param Environment $twig Instancia de twig
	 *
	 * @return Environment Instancia modificada que incluye horas los filtros y funciones
	 */
	public function addToTwig($twig)
	{
		/* this is where you can add your own fuctions to twig */
		try {
			$twig->addFunction(new TwigFunction('html_classes', [$this, 'html_classes']));
			$twig->addFunction(new TwigFunction('pagenavi', [$this, 'pagenavi']));
			$twig->addFunction(new TwigFunction('breadcrumb', [$this, 'breadcrumb']));
			$twig->addFunction(new TwigFunction('reusable_block', [$this, 'reusable_block']));
			$twig->addFilter(new TwigFilter('date_i18n', [$this, 'date_i18n']));
			$twig->addFilter(new TwigFilter('format_content', [$this, 'format']));
			$twig->addFilter(new TwigFilter('adjust', [$this, 'adjust']));
			$twig->addFilter(new TwigFilter('srcset_style', [$this, 'srcset_style']));
			$twig->addFilter(new TwigFilter('related_posts', [$this, 'related_posts']));
			$twig->addFilter(new TwigFilter('skip_tabs', [$this, 'skip_tabs']));
			// $twig->addFilter(new TwigFilter('wp_dropdown_categories', function ($item, $args) {
			// 	$args['echo'] = false;
			// 	$args['selected'] = $item;

			// 	return wp_dropdown_categories($args);
			// }));
		} catch(Exception $e) {
			// Solo atendemos los errores por si ya están estas funciones activas
		}

		return $twig;
	}
}
