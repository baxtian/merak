<?php

namespace Baxtian;

use Baxtian\MerakInterface;

// Inicializar Plugin
class MerakPlugin implements MerakInterface
{
	use MerakTrait;
	public const FILE    = __FILE__;
	public const DIR     = __DIR__;
	public const DOMAIN  = '';
	public const PREFIX  = '';
	public const VERSION = '';
	public const CONTEXT = [];
	public const TYPE    = 'plugin';

	/**
	 * Inicializa las acciones y filtros requeridos por Baxtian.
	 */
	public function __construct()
	{
		// Declaración de acciones
		add_action('plugins_loaded', [$this, 'plugin_textdomain']);
		add_filter('editable_extensions', [$this, 'editable_extensions'], 10, 2);

		$this->merak_actions();
	}

	/**
	 * Retornar la URL del directorio del plugin.
	 */
	public function url_path(): string
	{
		return dirname(plugin_dir_url($this->file));
	}

	/**
	 * Retornar el path del directorio del plugin.
	 */
	public function dir_path(): string
	{
		return plugin_dir_path($this->dir);
	}

	/**
	 * Retorna el path del directorio i18n.
	 */
	public function languages_dir(): string
	{
		return dirname(plugin_dir_path($this->file)) . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR;
	}

	/**
	 * Función para ser llamada después de activar los plugins.
	 */
	public function plugin_textdomain(): void
	{
		//Activar el traductor
		load_plugin_textdomain($this->domain, false, basename(dirname($this->file, 2)) . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR);
	}

	/**
	 * Permitir usar el editor de plugins para editar archivos twig.
	 *
	 * @param array  $default_types
	 * @param string $plugin
	 *
	 * @return array
	 */
	public function editable_extensions($default_types, $plugin)
	{
		if (!in_array('twig', $default_types)) {
			$default_types[] = 'twig';
		}

		return $default_types;
	}
}
