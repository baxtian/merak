<?php

namespace Baxtian;

use Timber\Timber;

// Inicializar Plugin
class MerakFlyImageResize
{
	public static function add_hooks()
	{
		add_filter('wp_prepare_attachment_for_js', [$this, 'prepare_attachment_for_js'], 10, 3);
		add_filter('the_content', [$this, 'imageInHtml']);
		add_filter('post_thumbnail_html', [$this, 'imageInHtml']);
		add_filter('get_header_image_tag', [$this, 'imageInHtml']);
		add_filter('admin_post_thumbnail_html', [$this, 'imageInHtml']);

		// Usar url de la imagen antes de cargar la información en el editor
		add_filter('media_send_to_editor', [$this, 'imageInHtml']);

		// Buscar los archivos fly para el editor
		add_filter('content_edit_pre', [$this, 'imageInHtml']);

		// Volver a los originales al guardar.
		// Así aseguramos que no se pierdan los valores originales y que al desactivar
		// el tema no se pierdan los enlaces a las imágenes
		add_filter('content_save_pre', [$this, 'revertOriginalContent']);

		// En caso de estar instalado el plugin de hotspot
		// forzar la eliminación de la imagen para crear los nuevos
		// thumbs si se crea un nuevo hotspot
		add_action('wp_ajax_hotspot_save', [$this, 'hotspot'], 9);
	}

	/**
	 * Organiza el arreglo de información de un attachment para los scripts
	 * del grid de imágenes. Solo los crea si ya no existen en el directorio
	 * de imágenes y solo usa los tamaños comunes (medium y thumbnail).
	 * Este procedimiento asegura que se tendrá el arreglo de imágenes para ser
	 * consultado posteriormente.
	 * @param  array       $response   Conjunto de datos adjuntos preparados.
	 * @param  WP_Post     $attachment Entrada adjunto
	 * @param  array|false $meta       Matriz de metadatos de datos adjuntos, o falsa si no la hay.
	 * @return array                   Arreglo con los cambios requeridos
	 */
	public function prepare_attachment_for_js($response, $attachment, $meta)
	{
		//Datos del directorio de archivos
		$dir = wp_upload_dir();
		if (isset($response['sizes']) && count($response['sizes']) > 0) {
			foreach ($response['sizes'] as $size_key => $size_data) {
				//A partir de la URL de la imagen buscamos el path en el directorio del servidor
				$file = str_replace($dir['baseurl'], $dir['basedir'], $size_data['url']);
				//Si la imagen no existe entonces hacemos uso de fly.
				if (!file_exists($file)) {
					// Recorrer el arreglo e tamaños disponibles para el attachment
						if ($size_key == 'medium' || $size_key == 'thumbnail') { // solo ejcutar acciones si es uno de los tamaños usados en el grid
							//Crear la versión "fly" de la imagen.
							$fly_image                    = fly_get_attachment_image_src($response['id'], [$size_data['width'], $size_data['height']]);
							$size_data['url']             = $fly_image['src'];
							$response['sizes'][$size_key] = $size_data;
						}
				}
			}
		}

		return $response;
	}

	/**
	 * Modificar los datos del contenido de una nota para cambiar las imágenes
	 * por sus versiones "fly".
	 * @param  string $content Contenido a ser editado
	 * @return string          Contenido modificado
	 */
	public function imageInHtml($content)
	{
		// Determinar el patrón a buscar usando el directorio de subida y una extensión para imágenes
		$dir     = wp_upload_dir();
		$baseurl = $dir['baseurl'];
		$regexp  = "#{$baseurl}/([\d]*)/([\d]*)/([\d\w\.-]*)-([\d]+)x([\d]+)\.(?:png|jpe?g|gif)#";

		// Buscar el patrón y reemplazar
		$content = preg_replace_callback(
			$regexp,
			function ($matches) {
				global $wpdb;

				//Si el archivo existe, no hay que hacer uso de fly
				$dir  = wp_upload_dir();
				$file = str_replace($dir['baseurl'], $dir['basedir'], $matches[0]);
				if (file_exists($file)) {
					return $matches[0];
				}

				// Teniendo los datos de nombre y extensión podemos saber el nombre oriinal de la imagen
				// y buscarla en  los GUID de los posts para tener su ID
				$image_url = preg_replace("#-([\d]+x[\d]+)\.(png|jpe?g|gif)#", '.$2', $matches[0]);
				// Buscamos el id a partir del guid
				$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
				//COnociendo el id y el tamaño podemos indicar el valor que eemplazará el match de esta consulta regexp
				return $this->adjust($attachment[0], $matches[4], $matches[5]);
			},
			$content
		);

		return $content;
	}

	/**
	 * Modificar los datos de un texto para cambiar las imágenes "fly" por sus
	 * versiones originales. Así se asegura que los datos almacenados en el content
	 * sean siempre los originales.
	 * @param  string $content Contenido a ser editado
	 * @return string          Contenido modificado
	 */
	public function revertOriginalContent($content)
	{
		//Determinar el directorio de las imágenes "fly"
		$dir        = wp_upload_dir();
		$basedir    = $dir['basedir'];
		$fly_images = \JB\FlyImages\Core::get_instance();
		$dir        = str_replace($basedir, '', $fly_images->get_fly_dir());

		// Determinar el patrón de búsqeuda según el directorio "fly" (solo la caprtea dentro del directorio de uploads), el id del
		// attachment, las diemnsione y la extensión de imágenes
		$regexp = "%{$dir}/(?<id>[\d]*?)/(?<image>[^\",\s]*?)(?<dim>-\d+x\d+)?(?:@\d)?\.(?<ext>png|jpe?g|gif)%";

		return preg_replace_callback(
			$regexp,
			function ($matches) {
				$dir     = wp_upload_dir();
				$baseurl = $dir['baseurl'];

				// Obtener el guid del id del attachment obtenido con el patrón de búsqueda
				$guid = get_the_guid($matches['id']);

				//Cambiar en la URL le título de la imagen paa incluir las dimensions y eliminar el directorio "fly"
				return str_replace([$baseurl, $matches['image']], ['', $matches['image'] . $matches['dim']], $guid);
			},
			$content
		);
	}

	/**
	 * Acciones al almacenarse un punto de foco de una imagen
	 * Requiere plugin My Eyes are up Here
	 *
	 * @return void
	 */
	public function hotspot()
	{
		//ID del elemento cuyo hotspot se está editando
		$att_id = isset($_POST['attachment_id']) ? intval($_POST['attachment_id']) : false;

		//Si tenemos ID
		if ($att_id) {
			//Actualizar la marca del hostspot
			update_post_meta($att_id, 'hotspot_timestamp', current_time('timestamp'));

			if (function_exists('fly_get_attachment_image_src')) {
				//Eliminar el directorio de fly_images si existe
				$fly_images = \JB\FlyImages\Core::get_instance();
				$fly_images->delete_attachment_fly_images($att_id);
			}
		}
	}

	/**
	 * Filtro para insertar la URL de una imagen fly en Timber
	 * @param  int|TimberImage $item   Identificador de la imagen o el objeto TimberImage.
	 * @param  int             $width  Ancho de la imagen.
	 * @param  int|false       $height Alto de la imagen o false para calcular manteniendo la relación.
	 * @return string|false            URL de la imagen o false si no pudo crear la imagen.
	 */
	public function adjust($item, $width, $height = false)
	{
		//Si el item es un número, entonces obtener el objeto TimberPost a partir del id.
		if (is_numeric($item)) {
			$item = Timber::get_post($item);
		}

		//Detectar que sea imagen, sino, retornar vacío
		if (!isset($item->post_type) || $item->post_type != 'attachment') {
			return false;
		}

		//Buscar el tamaño
		$crop = true;
		$size = $width; //Podría ser el descriptor del tamaño
		if (is_numeric($width)) { //Si es un número, entonces es por el momento solo el ancho
			if ($height === false || $height == 0) {
				//Si no hay ancho o es 0, entonces la altura se calcula según la
				//relación WxH de la imagen por defecto
				$size = [$width,0];
				$crop = false;
			} elseif (is_numeric($height)) {
				// Si ambos datos son numércos entonces debemos recortar la imagen
				// según el tamaño indicado
				$size = [$width, $height];
				$crop = true;
			}
		}

		//Buscar la imagen con el tamaño solicitado
		$answer = '';
		if (function_exists('fly_get_attachment_image_src')) {
			//Si tenemos fly-image-resizer, usar este plugin
			$answer = fly_get_attachment_image_src($item->id, $size, $crop);
			if (is_array($answer)) {
				$answer = $answer['src'];
			}
		} else {
			//Si no tenemos fly image resizer, usar Timber
			$img    = new Timber\Image($item->id);
			$answer = Timber\ImageHelper::resize($img->src, $size[0], $size[1]);
			if (is_array($answer)) {
				$answer = $answer[0];
			}
		}

		// //Buscamos la URL en la respuesta
		// if (is_array($answer)) {
		// 	$answer = $answer[0];
		// }

		//Si hay hotspot_timestamp entonces ponerlo al final para forzar usar la imagen
		//con el hotspot aplicado

		if ($timestamp = get_post_meta($item->ID, 'hotspot_timestamp', true)) {
			$answer .= '?timestamp=' . $timestamp;
		}

		return $answer;
	}
}
