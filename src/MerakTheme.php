<?php

namespace Baxtian;

use Baxtian\MerakInterface;
use Baxtian\MerakFlyImageResize;
use Timber\Timber;
use Timber\Site;
use Timber\LocationManager;
use Timber\Loader;

// Inicializar Plugin
class MerakTheme extends Site implements MerakInterface
{
	use MerakTrait;
	public const FILE    = __FILE__;
	public const DIR     = __DIR__;
	public const DOMAIN  = '';
	public const PREFIX  = '';
	public const VERSION = '';
	public const CONTEXT = [];
	public const TYPE    = 'theme';

	public function __construct()
	{
		// Como este es el tema, es nuestro deber inicializar Timber
		// Pero para evitar problemas con el plugin viejo de timber
		// solo se inicializará si aun no se ha inicializad timber 
		if(!defined('TIMBER_LOADED')) {
			Timber::init();
		}

		// Vincular los hooks
		$this->add_hooks();

		// Continuar con la declaración
		parent::__construct();
	}

	/**
	 * Inicializa las acciones y filtros requeridos por Baxtian.
	 */
	public function add_hooks()
	{
		// Declaración de filtros y acciones
		add_action('after_setup_theme', [$this, 'setup']);

		// Corregir errores en las direcciones de los scripts a ser traducidos
		// Bug propio de WP - Posiblemente se correija en un futuro.
		add_filter('load_script_translation_file', [$this, 'script_translation_file'], 10, 3);

		// Tipos de archivos editables por el wp_editor
		add_filter('wp_theme_editor_filetypes', [$this, 'editor_filetypes'], 10, 2);

		// Delarar uso de filtros para bloques con timber
		add_filter('timber_block', [$this, 'timber_block'], 10, 3);

		// Filtro de contexto de bloques vacìos
		add_filter('render_block_context', [$this, 'block_context'], 10, 3);

		// Agregamos la integración fly-resize si está instalada
		if (function_exists('fly_add_image_size')) {
			MerakFlyImageResize::add_hooks();
		}

		$this->merak_actions();
	}

	/**
	 * Retornar la URL del directorio del plugin.
	 */
	public function url_path(): string
	{
		return get_template_directory_uri();
	}

	/**
	 * Retornar el path del directorio del plugin.
	 */
	public function dir_path(): string
	{
		return get_template_directory() . DIRECTORY_SEPARATOR ;
	}

	/**
	 * Retorna path del directoro con los archivos de traducción.
	 *
	 * @return string
	 */
	public function languages_dir(): string
	{
		return get_template_directory(dirname($this->dir)) . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR;
	}

	/**
	 * Corregir la situación del archivo de idioma si es javascript
	 * Por defecto los archivos de traducción inlcuyen el nombre del dominio como
	 * un prefijo, pero por tratarse de una plantilla este no es el comportamiento
	 * esperado. Esta función retira estos prefijos para que el archivo sí tenga el
	 * nombre correcto con el que se busca en las funciones de traducción.
	 *
	 * @param string $file  Archivo a ser traducido
	 * @param string $handle Nombre con el que se identificó al script
	 * @param string $domain Dominio usado para l atraducción
	 *
	 * @return string Nombre real del archivo a ser traducido
	 */
	public function script_translation_file($file, $handle, $domain)
	{
		if ($domain == static::DOMAIN) {
			$file = str_replace(static::DOMAIN . '-', '', $file);
		}

		if($domain == static::DOMAIN && $handle == static::PREFIX . "_blocks") {
			$hash = md5('build/gtmbrg/block.js');
			$file = str_replace('bxt_blocks', $hash, $file);
			$file = str_replace(static::DOMAIN . '-', '', $file);
		}

		return $file;
	}

	/**
	 * Filtro para permitir editar twigs desde wp_editor.
	 *
	 * @param array $default_types Arreglo con las extensiones d elos archivos editables
	 * @param string $theme     Nobre del tema
	 *
	 * @return array Arreglo con los nuevos tipos permitidos
	 */
	public function editor_filetypes($default_types, $theme)
	{
		$default_types[] = 'twig';

		return $default_types;
	}

	/**
	 * Atender bloques que usen el filtro timber_block y permitir así tener
	 * plantillas timber propias para los bloques. Si no hay archivo de plantilla
	 * que concuerde con tpl entonces se retorna la cadena text que ingresó por defecto.
	 *
	 * @param string $text    Respuesta actual del bloque
	 * @param string $tpl    Plantilla twig a ser buscada
	 * @param array $attributes Atributos a ser enviados a la plantilla
	 *
	 * @return string Cadena con el html generado
	 */
	public function timber_block($text, $tpl, $attributes)
	{
		// Revisar que exista el archivo
		$file   = $this->dir . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $tpl;
		$answer = false;
		if (file_exists($file)) {
			$args = array_merge(Timber::context(), $attributes);

			$caller = LocationManager::get_calling_script_dir(1);
			$loader = new Loader($caller);
			$file   = $loader->choose_template($tpl);
			if ($file) {
				$answer = $this->render($tpl, $args, false);
				if (empty($answer)) {
					$answer = $text;
				}
			}
		}

		return $answer;
	}

	/**
	 * Agregar contexto a bloques vacíos. En timber al no usar el Loop
	 * esta información no se carga adecuadamente asíq ue debemos hacerlo
	 * por esta vía.
	 *
	 * @param array $context
	 * @param array $parsed_block
	 * @param array $parent_block
	 * @return array
	 */
	public function block_context($context, $parsed_block, $parent_block)
	{
		// Si trae blockName, es una página singular, no hay contexto y es un bloque 'core/post-'
		// usar en el contexto el id del post actual
		if (!empty($parsed_block['blockName']) && is_singular() && empty($context) && strstr($parsed_block['blockName'], 'core/post-') !== false ) {
			global $post;
			$context['postId'] = $post->ID;
		}

		return $context;
	}

	/**
	 * Obtener las etiquetas OG para la cabecera.
	 *
	 * @return string
	 */
	public function get_og($default_image = null)
	{
		global $post, $og;

		// Default image
		$image = (!empty($default_image)) ? $default_image : get_bloginfo('stylesheet_directory') . '/screenshot.png';

		if (is_home() || is_front_page()) {
			$text  = get_bloginfo('description');
			$title = get_bloginfo('name');
			$type  = 'website';
			$url   = get_bloginfo('url');
			if (has_post_thumbnail() && get_post_thumbnail_id()) {
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
				$image = (is_array($image)) ? $image[0] : false;
			}
		} elseif (is_single() || is_page()) {
			$title = $post->post_title;
			$url   = get_permalink($post->ID);
			$type  = 'article';

			if (has_post_thumbnail()) {
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
				if(isset($image[0])) $image = $image[0];
			}
			$text = strip_tags(get_the_excerpt($post));
		} elseif (is_archive()) {
			$title = wp_title('&raquo;', false, 'right');
			$title = explode('&raquo;', $title);
			$title = trim($title[0]);
			$url   = get_post_type_archive_link('post');
			$type  = 'archive';
			$text  = sprintf(__('Archive: %s', 'merak'), $title);
		} else {
			$text  = get_bloginfo('description');
			$title = get_bloginfo('name');
			$type  = 'website';
			$url   = get_bloginfo('url');
		}

		$answer = [
			'title'       => trim(strip_tags($title)),
			'type'        => $type,
			'url'         => $url,
			'image'       => $image,
			'site_name'   => get_bloginfo('name'),
			'description' => trim(strip_tags($text)),
			'html'        => '',
		];

		$answer = apply_filters('get_og', $answer);

		$answer['html'] = sprintf('<meta name="description" content="%s" />' . PHP_EOL, trim(strip_tags($answer['description'])));
		$answer['html'] .= sprintf('<meta property="og:title" content="%s" />' . PHP_EOL, trim(strip_tags($answer['title'])));
		$answer['html'] .= sprintf('<meta property="og:type" content="%s" />' . PHP_EOL, $answer['type']);
		$answer['html'] .= sprintf('<meta property="og:url" content="%s" />' . PHP_EOL, $answer['url']);
		$answer['html'] .= sprintf('<meta property="og:image" content="%s" />' . PHP_EOL, $answer['image']);
		$answer['html'] .= sprintf('<meta property="og:site_name" content="%s" />' . PHP_EOL, get_bloginfo('name'));
		$answer['html'] .= sprintf('<meta property="og:description" content="%s" />' . PHP_EOL, trim(strip_tags($answer['description'])));

		return $answer;
	}
}
