<?php

namespace Baxtian;

use Timber\Timber;
use Baxtian\MerakTimber;
use Twig\Environment;

// Inicializar Plugin
trait MerakTrait
{

	protected $assets_types = [
		'frontend' => [],
		'admin'    => [],
		'login'    => [],
	];

	public function __get($key)
	{
		switch ($key) {
			case 'dir':
				return static::DIR;

				break;
			case 'file':
				return static::FILE;

				break;
			case 'domain':
				return static::DOMAIN;

				break;
			case 'prefix':
				return static::PREFIX;

				break;
			case 'version':
				return static::VERSION;

				break;
			case 'context':
				return static::CONTEXT;

				break;
			case 'type':
				return static::TYPE;

				break;
		}

		return false;
	}

	protected function merak_actions()
	{
		// Registro de librerías y estilos
		add_action('wp_loaded', [$this, 'register_assets']);

		// Registro librerías para el backend
		add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_assets']);

		// Registro librerías para el formulario de ingreso
		add_action('login_enqueue_scripts', [$this, 'login_enqueue_assets']);

		// Registro librerías para el frontend
		add_action('wp_enqueue_scripts', [$this, 'enqueue_assets']);

		// Variables de contexto ara twig en Merak
		add_filter('timber/context', [$this, 'add_to_context']);

		// Funciones y filtros twig
		add_filter('timber/twig', [$this, 'add_to_twig']);

		// Directorio y enlace para ajuste de tamaño de imágenes compiladas en el tema
		add_filter('timber/image/new_url', [$this, 'timber_resize_url'], 10, 1);
		add_filter('timber/image/new_path', [$this, 'timber_resize_path'], 10, 1);

		// Traducciones en esta librería
		add_action('plugins_loaded', [$this, 'i18n']);

		// Incluir traducción
		$locale = apply_filters('plugin_locale', determine_locale(), static::DOMAIN);
		$path = dirname(__DIR__, 1) . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR;
		$mofile = $path . 'merak-' . $locale . '.mo';
		load_textdomain('merak', $mofile);
	}

	/**
	 * Inicialización del sistema de traducción
	 *
	 * @return void
	 */
	public function i18n()
	{
		$domain = 'merak';
		$locale = apply_filters('plugin_locale', determine_locale(), $domain);

		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$path         = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile       = $path . $domain . '-' . $locale . '.mo';

		load_textdomain($domain, $mofile);
	}

	/**
	 * Filtro para agregar los directorios de plantillas de plugins
	 *
	 * @param array $paths Arreglo con directorios de plantillas
	 * @return void Se agrega el directorio de plantillas del tema/plugin
	 * 				actual si aun no se ha agregado
	 */
	public function timber_loader_paths($paths): array
	{
		// Directorio de la plantilla
		$template_directory = $this->dir_path() . 'templates' . DIRECTORY_SEPARATOR;

		// Si el directorio no está en el arreglo de paths y paths es un arreglo
		if (is_array($paths) && !in_array($template_directory, $paths)) {
			// Si existe el directorio, agregarlo
			if (file_exists($template_directory)) {
				$paths[] = [ $template_directory ];
			}
		}

		return $paths;
	}

	/**
	 * Registra un estilo o script
	 *
	 * @param string $handle
	 * @param string $name
	 * @param string $type
	 * @param array $dependencies
	 * @param bool $enqueue
	 * @return boolean estado del registro
	 */
	protected function register_asset($handle, $name, $type, $dependencies = [], $enqueue = true)
	{
		// Determinar el tipo correcto
		if (!in_array($type, ['frontend', 'admin', 'login'])) {
			$type = 'frontend';
		}

		$asset_file = (file_exists($this->dir_path() . '/build/assets.version.php')) ? include($this->dir_path() . '/build/assets.version.php') : false;
		$version    = $asset_file['version'] ?? $this->version;

		if (file_exists($this->dir_path() . "/build/{$name}.asset.php")) {
			$asset_file   = include($this->dir_path() . "/build/{$name}.asset.php");
			$version      = $asset_file['version'] ?? $this->version;
			$dependencies = isset($asset_file['dependencies']) ? array_merge($asset_file['dependencies'], $dependencies) : $dependencies;
		}

		if (file_exists($this->dir_path() . "/build/{$name}.css")) {
			if ($enqueue) {
				$this->assets_types[$type]['style'][] = $handle;
			}

			return wp_register_style($handle, $this->url_path() . "/build/{$name}.css", $dependencies, $version);
		} elseif (file_exists($this->dir_path() . "/build/{$name}.js")) {
			if ($enqueue) {
				$this->assets_types[$type]['script'][] = $handle;
			}

			return wp_register_script($handle, $this->url_path() . "/build/{$name}.js", $dependencies, $version);
		}

		return false;
	}

	protected function register_assets()
	{
		// Cargar las librerías extra
		// Inicializar elementos declarados en la plantilla en el directorio de código
		$libs     = [];
		$filename = $this->dir_path() . 'build/libs/lib.json';
		if (file_exists($filename)) {
			// Version
			$version = $this->version;

			// Extraer la información
			$libs = json_decode(file_get_contents($filename), true);
			if (is_array($libs) && count($libs) > 0) {
				foreach ($libs as $file => $lib) {
					// Ubicación del archivo.
					// Suponemos inicialmente qiue es local
					$url = $this->url_path() . "/build/libs/{$file}";
					// Si tiene http:// o https:// o // entonces es externo
					if (
						strpos($file, 'http://') !== false ||
						strpos($file, 'https://') !== false ||
						strpos($file, '//') !== false
					) {
						$url = $file;
					} elseif (substr_compare($file, '.scss', -5) === 0) {
						$url = str_replace('.scss', '.css', $url);

						// Revisar si existe el archivo asset del archivo compilado
						$asset = $this->dir_path() . "/build/libs/{$file}.asset.php";
						$asset = str_replace('.scss', '', $asset);
						if (file_exists($asset)) {
							$asset_file = include($asset);
							$version    = $asset_file['version'];
						}
					}

					// Dependiendo del tipo se registra como estilo o como script
					if ($lib['type'] == 'style') {
						wp_register_style($lib['handle'], $url, $lib['dependencies'], $version);
					} elseif ($lib['type'] == 'script') {
						wp_register_script($lib['handle'], $url, $lib['dependencies'], $version);
					}
				}
			}
		}
	}

	/**
	 * Estilos y scripts a ser llamados en el backend.
	 */
	public function admin_enqueue_assets(): void
	{
		if (!empty($this->assets_types['admin']['script'])) {
			foreach ($this->assets_types['admin']['script'] as $handle) {
				wp_enqueue_script($handle);
			}
		}

		if (!empty($this->assets_types['admin']['style'])) {
			foreach ($this->assets_types['admin']['style'] as $handle) {
				wp_enqueue_style($handle);
			}
		}

		// Inicializar bloques manualmente si nadie los ha requerido aun
		// wp_enqueue_style(EQT_P . '_admin');
		// wp_enqueue_script(EQT_P . '_blocks');
	}

	/**
	 * Estilos y scripts a ser llamados en el frontend.
	 */
	public function enqueue_assets(): void
	{
		if (!empty($this->assets_types['frontend']['script'])) {
			foreach ($this->assets_types['frontend']['script'] as $handle) {
				wp_enqueue_script($handle);
			}
		}

		if (!empty($this->assets_types['frontend']['style'])) {
			foreach ($this->assets_types['frontend']['style'] as $handle) {
				wp_enqueue_style($handle);
			}
		}

		// Inicializar bloques manualmente si nadie los ha requerido aun
		// wp_enqueue_style(EQT_P . '_block-style');
		// wp_enqueue_script(EQT_P . '_block-frontend');
	}

	/**
	 * Estilos y scripts a ser llamados en el login.
	 */
	public function login_enqueue_assets(): void
	{
		if (!empty($this->assets_types['login']['script'])) {
			foreach ($this->assets_types['login']['script'] as $handle) {
				wp_enqueue_script($handle);
			}
		}

		if (!empty($this->assets_types['login']['style'])) {
			foreach ($this->assets_types['login']['style'] as $handle) {
				wp_enqueue_style($handle);
			}
		}

		// wp_enqueue_style( MRKTAX_P . '_login' );
	}

	/**
	 * Variables que son generales a todo el sitio y serán vinculadas como parte del context de Timber.
	 *
	 * @param array $context Arreglo con las variables de contexto que incluye timber por defecto
	 *
	 * @return array El arreglo de contexto incluye las variables propias de esta plantilla
	 */
	public function add_to_context($context)
	{
		//Plugin data from file
		if ($this->context) {
			$data    = $this->context;
			$keys    = array_keys($data);
			$context = array_merge(
				$context,
				$data,
				[
					$data[$keys[2]] => [
						'link' => $this->url_path(),
					],
				]
			);
		}

		if ($this->type == 'theme') {
			$context['site'] = $this;
		}

		return $context;
	}

	/**
	 * Agregar funciones y filtros al twig
	 *
	 * @param \Twig\Environment $twig
	 * @return \Twig\Environment
	 */
	public function add_to_twig(Environment $twig): Environment
	{
		$twig = MerakTimber::get_instance()->addToTwig($twig);

		return $twig;
	}

	/**
	 * Función para renderizar una plantilla.
	 * Requiere tener activo el plugin Timber.
	 * Lea la documentación de Timber en http://timber.github.io/timber/
	 * Timber está basado en Twig.
	 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/.
	 *
	 * @param string $tpl Plantilla twig a ser usada
	 * @param array  $args   Arregloc on los argumentos a ser enviados a la plantilla
	 * @param bool   $echo   Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
	 *                       la información directamente en vez de imprimirla. Por defecto es true.
	 *
	 * @return bool|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
	 *                     cadena con el texto renderizado o false si no pudo ejecutar la acción
	 */
	public function render($tpl, $args, $echo = true): string
	{
		// Declarar el directorio de plantillas twig (templates o views)
		$locations = [];
		if (file_exists($this->dir_path() . DIRECTORY_SEPARATOR . 'templates')) {
			$locations[] = $this->dir_path() . DIRECTORY_SEPARATOR . 'templates';
		}
		if (file_exists($this->dir_path() . DIRECTORY_SEPARATOR . 'views')) {
			$locations[] = $this->dir_path() . DIRECTORY_SEPARATOR . 'views';
		}
		Timber::$locations = $locations;

		// Obtener contexto para timber
		$context = Timber::context();

		// Datos del plugin
		if ($this->type == 'plugin') {
			$context['plugin'] = ['link' => plugin_dir_url($this->dir_path())];
		}

		// Incluir datos de contexto de este elemento 
		// en caso de no haber sido agregados aun
		$context = array_merge($context, $this->context);

		// Incluir el directorio de este elemento en Timber
		add_filter('timber/locations', [$this, 'timber_loader_paths'], 10, 1);

		// Render
		$answer = ($echo) ?
			Timber::render($tpl, array_merge($context, $args)) :
			Timber::compile($tpl, array_merge($context, $args));

		// Retirar el directorio de este elemento de Timber
		remove_filter('timber/locations', [$this, 'timber_loader_paths'], 10);

		return ($answer) ? $answer : '';
	}

	/**
	 * Directorio de compilación para imágenes que serán ajustadas por la plantilla
	 *
	 * @param string $path
	 * @return string
	 */
	public function timber_resize_path($path)
	{
		return $this->timber_resize_filter($path, DIRECTORY_SEPARATOR);
	}

	/**
	 * Directorio de compilación para imágenes que serán ajustadas por la plantilla
	 *
	 * @param string $path
	 * @return string
	 */
	public function timber_resize_url($path)
	{
		return $this->timber_resize_filter($path, '/');
	}

	/**
	 * Directorio de compilación para imágenes que serán ajustadas por la plantilla
	 *
	 * @param string $path
	 * @return string
	 */
	private function timber_resize_filter($path, $separator)
	{
		// Si es un archivo del directorio de archivos compilados
		// y es del tipo de esta instancia (theme o plugin)
		if (
			strpos($path, 'build' . $separator . 'media' . $separator) !== false &&
			strpos($path, $separator . $this->type . 's' . $separator) !== false
		) {
			$pathname = $separator . 'build' . $separator . $this->type . $separator . $this->prefix . $separator;

			if (is_multisite()) {
				$blog_id = get_current_blog_id();
				$pathname .= $blog_id . $separator;
			}

			// Si inicia con http es url
			$build = (strpos($path, 'http') === 0) ? WP_CONTENT_URL . $pathname : WP_CONTENT_DIR . $pathname;

			$explode = explode(sprintf('build%smedia%s', $separator, $separator), $path);
			$path    = $build . $explode[1];
		}

		return $path;
	}
}
