# DESCRIPTION

Class to be inherite to add merak requirements.

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

### 0.5.31

* Solving bug that removes @kaeyframes while compiling svg to use custom colors.

### 0.5.30

* i18n improvements for theme blocks and library translation

### 0.5.28

* Add taxonomy archive to breadcrumb

### 0.5.26

* Set minimum php verion to 7.4.

### 0.5.25

* Mejorar breadcrumb.

### 0.5.23

* Solving warnings with Timber2.

### 0.5.22

* Changes in filter 'adjust' for Timber2.

### 0.5.21

* Initializing Timber if this is class is beeing called by a theme.

### 0.5.20

* Solving bugs.

### 0.5.14

* Use try/catch inside twig-functions declaration.

### 0.5.13

* Recover timber managment.

### 0.5.12

* Use direct filename for rendering.

### 0.5.11

* Use loader/paths filter only during rendering.

### 0.5.10

* Fix bugs

### 0.5.9

* Add dependencies included in the assets file for each asset aded with register_asset.
* Allow to indicate if the asset requires to be enquede. True by default

### 0.5.8

* Adjusts to include multiple plugin paths to templates.

### 0.5.7

* Add nl2br to format_content.

### 0.5.6

* Fix bug with og graph.

### 0.5.5

* Fix warnings with PHP 8.1.

### 0.5.4

* i18n.

### 0.5.3

* Solve bugs.

### 0.5.2

* Remove theme/plugin template dir if it does not exists.

### 0.5.1

* Allow svg to use theme sass vars.

### 0.4.19

* Add svg-transform-loader.

### 0.4.18

* Add context to 'core/post-*' blocks.

### 0.4.17

* Use strstr instead str_contains

### 0.4.16

* Add filter to use the templates directory fro this teme or plugin.

### 0.4.14

* Return false if post hast thumbnail but image dos not exists.

### 0.4.13

* Detect if image to be resize is in the directory of current type (themes or plugins).

### 0.4.12

* Cache directory where to create copies of the asset images that have been copied to the build directory.

### 0.4.11

* Recover adst function in twig.

### 0.4.10

* Add function add_to_twig.

### 0.4.9

* Include site in context.

### 0.4.8

* Include prefix.link to context.

### 0.4.7

* Allow to include json files bessides lib.json in assets/libs.

### 0.4.6

* Bug file name for FlyImageResize.

### 0.4.5

* Recover timber data for themes.

### 0.4.4

* Allow to define WP_ENTRY before this script.

### 0.4.3

* Using static instead self also in plugins.

### 0.4.2

* Using static instead self.

### 0.4.1

* Solving bug with directories.

### 0.4

* Using constants instead variables.

### 0.3

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

### 0.2.10

* Detect if assets file exists

### 0.2.9

* Solve bug with build

### 0.2.8

* Use array instead class to set version

### 0.2.7

* Use webpack-php-asset-versioning when in develpoment

### 0.2.6

* Use webpack-php-asset-versioning

### 0.2.5

* Changes to allow use new webpack config

### 0.2.4

* Include separator at the end of the PATH.

### 0.2.3

* Warning before foreach.

### 0.2.2

* Allow call instances file.

### 0.2.1

* Set dynamic version for scss styles inside libs.

### 0.2.0

* First stable release.

### 0.1.0

* First release.
