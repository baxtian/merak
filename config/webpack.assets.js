const fs = require('fs');

function AssetsVersioning (options) {
  this.options = options || {}
}

AssetsVersioning.prototype.apply = function apply (compiler) {

  const options = this.options;
  const file_name = this.options.file_name || "./assets.version.php"

    compiler.hooks.done.tap('Assets Version', () => {
        const stamp = Date.now();
        const content =   '<?php return [ \'version\' => '+ stamp +' ];';

        fs.writeFile(file_name, content, (err) => {
            if (err) throw err;
            console.log('Assets version updated');
        });
    });
};

module.exports = AssetsVersioning;
